/* © Copyright 2021 Gareth Anthony Hulse
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#pragma once

#define write_csr(reg, val) {asm ("csrw " #reg ", %0" :: "rK" (val));}

namespace matilda::cpu::sifive::e31
  {
    class util
    { 
    public:
      template <typename T, typename U,
		typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type,
		typename = typename std::enable_if<std::is_arithmetic<U>::value, U>::type>
      static inline constexpr void
      write_address (volatile T *address, const U &data)
      {
	T value = *address;
	value += data;
      
	*address = value;
      }

      static inline uint32_t
      read_csr (const uint32_t reg) __attribute__ ((always_inline))
      {
	volatile uint32_t data;
	asm ("csrr %0, %1" :
	     "=r" (data) :
	     "I" (reg));

	return data;
      }
    };
}
