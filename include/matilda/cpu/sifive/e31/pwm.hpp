/* © Copyright 2021 Gareth Anthony Hulse
   SPDX-License-Identifier: LGPL-3.0-or-later
*/

#pragma once

#include <array>

namespace matilda::cpu::sifive::e31
{  
  class pwm
  {
  private:
    uint8_t m_instance;
    
    std::array<uint32_t, 3>
    base =
      {
	0x10015000,
	0x10025000,
	0x10035000
      };

  public:
    pwm (const uint8_t instance = 0)
    {
      m_instance = instance;
    }
    
    struct bitmask
    {
      enum class cfg : uint32_t
      {
	enable_always = 1 << 12,
	cmp_1_center = 1 << 17,
	deglitch = 1 << 10,
	sticky = 1 << 8
      };
    };
    
    constexpr void
    cfg (const bitmask::cfg data, const uint8_t divider)
    {
      volatile uint32_t *address = reinterpret_cast<volatile uint32_t *> (base[m_instance]);
      if (static_cast<uint32_t> (data) > 0b1111 and divider < 0b1111)
	{
	  *address = static_cast<uint32_t> (data) + divider;
	}
    }

    constexpr void
    cmp_1 (const uint16_t data)
    {
      volatile uint32_t *address = reinterpret_cast<volatile uint32_t *> (base[m_instance] + 0x24);
      *address = data;
   }
  };
}

constexpr matilda::cpu::sifive::e31::pwm::bitmask::cfg operator
| (matilda::cpu::sifive::e31::pwm::bitmask::cfg a, matilda::cpu::sifive::e31::pwm::bitmask::cfg b)
{
  return static_cast<matilda::cpu::sifive::e31::pwm::bitmask::cfg> (static_cast<uint32_t> (a) | static_cast<uint32_t> (b));
}
