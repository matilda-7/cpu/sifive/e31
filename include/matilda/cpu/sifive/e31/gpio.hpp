/* © Copyright 2021 Gareth Anthony Hulse
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#pragma once

#include "util.hpp"

namespace matilda::cpu::sifive::e31
{
  class gpio : protected util
  {
  private:
    const uint32_t m_base = 0x10012000;
    volatile uint32_t
    *m_input_val = reinterpret_cast<uint32_t*> (m_base),
      *m_input_en = reinterpret_cast<uint32_t*> (m_base + 0x4),
      *m_output_en = reinterpret_cast<uint32_t*> (m_base + 0x8),
      *m_output_val = reinterpret_cast<uint32_t*> (m_base + 0xc),
      *m_pue = reinterpret_cast<uint32_t*> (m_base + 0x10),
      *m_ds = reinterpret_cast<uint32_t*> (m_base + 0x14),
      *m_rise_ie = reinterpret_cast<uint32_t*> (m_base + 0x18),
      *m_rise_ip = reinterpret_cast<uint32_t*> (m_base + 0x1c),
      *m_fall_ie = reinterpret_cast<uint32_t*> (m_base + 0x20),
      *m_fall_ip = reinterpret_cast<uint32_t*> (m_base + 0x24),
      *m_high_ie = reinterpret_cast<uint32_t*> (m_base + 0x28),
      *m_high_ip = reinterpret_cast<uint32_t*> (m_base + 0x2c),
      *m_low_ie = reinterpret_cast<uint32_t*> (m_base + 0x30),
      *m_low_ip = reinterpret_cast<uint32_t*> (m_base + 0x34),
      *m_iof_en = reinterpret_cast<uint32_t*> (m_base + 0x38),
      *m_iof_sel = reinterpret_cast<uint32_t*> (m_base + 0x3c),
      *m_out_xor = reinterpret_cast<uint32_t*> (m_base + 0x40);
    
  public:
    void
    input_val (const uint32_t &bits)
    {      
      write_address (m_input_val, bits);
    }

    auto
    get_input_val ()
    {
      return *m_input_val;
    }

    void
    input_en (const uint32_t &bits)
    {      
      write_address (m_input_en, bits);
    }

    auto
    get_input_en ()
    {
      return *m_input_en;
    }
    
    void
    output_en (const uint32_t &bits)
    {      
      write_address (m_output_en, bits);
    }

    auto
    get_output_en ()
    {
      return *m_output_en;
    }
    
    void
    output_val (const uint32_t &bits)
    {
      write_address (m_output_val, bits);
    }

    auto
    get_output_val ()
    {
      return *m_output_val;
    }

    void
    pue (const uint32_t &bits)
    {      
      write_address (m_pue, bits);
    }

    auto
    get_pue ()
    {
      return *m_pue;
    }

    void ds (const uint32_t &bits)
    {
      write_address (m_ds, bits);
    }

    auto
    get_ds ()
    {
      return *m_ds;
    }

    void rise_ie (const uint32_t &bits)
    {
      write_address (m_rise_ie, bits);
    }

    auto
    get_rise_ie ()
    {
      return *m_rise_ie;
    }

    void
    rise_ip (const uint32_t &bits)
    {
      write_address (m_rise_ip, bits);
    }

    auto
    get_rise_ip ()
    {
      return *m_rise_ip;
    }

    void
    fall_ie (const uint32_t &bits)
    {
      write_address (m_fall_ie, bits);
    }

    auto
    get_fall_ie ()
    {
      return *m_fall_ie;
    }

    void
    fall_ip (const uint32_t &bits)
    {
      write_address (m_fall_ip, bits);
    }

    auto
    get_fall_ip ()
    {
      return *m_fall_ip;
    }

    void
    high_ie (const uint32_t &bits)
    {
      write_address (m_high_ie, bits);
    }

    auto
    get_high_ie ()
    {
      return *m_high_ie;
    }

    void
    high_ip (const uint32_t &bits)
    {
      write_address (m_high_ip, bits);
    }

    auto
    get_high_ip ()
    {
      return *m_high_ip;
    }

    void
    low_ie (const uint32_t &bits)
    {
      write_address (m_low_ie, bits);
    }

    auto
    get_low_ie ()
    {
      return *m_low_ie;
    }

    void
    low_ip (const uint32_t &bits)
    {
      write_address (m_low_ip, bits);
    }

    auto
    get_low_ip ()
    {
      return *m_low_ip;
    }

    void
    iof_en (const uint32_t &bits)
    {
      write_address (m_iof_en, bits);
    }

    auto
    get_iof_en ()
    {
      return *m_iof_en;
    }

    void
    iof_sel (const uint32_t &bits)
    {
      write_address (m_iof_sel, bits);
    }

    auto
    get_iof_sel ()
    {
      return *m_iof_sel;
    }

    void
    out_xor (const uint32_t &bits)
    {
      write_address (m_out_xor, bits);
    }

    auto
    get_out_xor ()
    {
      return *m_out_xor;
    }
  };
}
